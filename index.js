const express = require("express");
const bodyParser = require("body-parser");

const { Profile, Phone } = require("./models");

const app = express();
const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });
const port = "3000";

app.set('view engine', 'ejs');

// Views
app.get("/", (req, res) => {
    Profile.findAll({
        where: {},
        order: [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ],
        include: Phone
    })
    .then(profiles => {
        res.render('profiles/index', {
            profiles: profiles
        });
    });
});

app.get("/create", (req, res) => {
    res.render('profiles/create');
});

app.get("/edit/:id", (req, res) => {
    Profile.findByPk(req.params.id)
    .then(profile => {
        res.render('profiles/edit', {
            profile: profile
        });
    });
});

app.get("/show/:id", (req, res) => {
    Profile.findByPk(req.params.id, {
        include: Phone
    })
    .then(profile => {
        res.render('profiles/show', {
            profile: profile
        });
    });
});

app.get("/show/:id/phone", (req, res) => {
    Profile.findByPk(req.params.id)
    .then(profile => {
        res.render('profiles/phones/create', {
            profile: profile
        });
    });
});

// API
app.post("/api/profile", urlEncoded, async (req, res) => {
    try {
        let profile = await Profile.create({
            name: req.body.name
        });

        res.redirect('/');
    } catch (error) {
        res.render('profiles/create', {
            error: error
        });
    }
});

app.post("/api/profile/edit", urlEncoded, async (req, res) => {
    try {
        let profile = await Profile.findByPk(req.body.id);
        profile.name = req.body.name ? req.body.name : profile.name;
        await profile.save();

        res.redirect('/');
    } catch (error) {
        res.render('profiles/edit', {
            error: error
        });
    }
});

app.get("/api/profile/delete/:id", async (req, res) => {
    try {
        const count = await Profile.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return res.redirect('/');
        } else {
            return res.redirect('/');
        }
    } catch (err) {
        return res.redirect('/');
    }

});

app.post("/api/profile/phone", urlEncoded, async (req, res) => {
    try {
        let phone = await Phone.create({
            profileId: parseInt(req.body.profileId),
            data: req.body.data
        });

        res.redirect(`/show/${req.body.profileId}`);
    } catch (error) {
        res.render('profiles/phones/create', {
            error: error,
            profile: Profile.findByPk(req.body.profileId)
        });
    }
});

// app.post("/api/profile/edit", urlEncoded, async (req, res) => {
//     try {
//         let profile = await Profile.findByPk(req.body.id);
//         profile.name = req.body.name ? req.body.name : profile.name;
//         await profile.save();

//         res.redirect('/');
//     } catch (error) {
//         res.render('profiles/edit', {
//             error: error
//         });
//     }
// });

// app.get("/api/profile/delete/:id", async (req, res) => {
//     try {
//         const count = await Profile.destroy({
//             where: { id: req.params.id }
//         });

//         if (count > 0) {
//             return res.redirect('/');
//         } else {
//             return res.redirect('/');
//         }
//     } catch (err) {
//         return res.redirect('/');
//     }

// });

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})